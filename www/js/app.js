angular.module('ionicApp', ['ionic'])
    .run(function($ionicPlatform) {
        Parse.initialize("p4xaVbs2LL5KSn4Hob9aegsKWpXQiGTwze1BLjQn", "84bgE4xWMyNGzoGRzAYhD4e6Q95zjDGSX6Yp3hkZ");
        $ionicPlatform.ready(function() {
            console.log('$ionicPlatform.ready');
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        })
    })
    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('signin', {
                url: '/sign-in',
                templateUrl: 'templates/sign-in.html',
                controller: 'SignInCtrl'
            })
            .state('home', {
                url: '/home/:role',
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            })
            .state('novoCliente', {
                url: '/cliente/novo',
                templateUrl: 'templates/novo-cliente.html',
                controller: 'NovoClienteCtrl'
            })
            .state('editarCliente', {
                url: '/cliente/:id',
                templateUrl: 'templates/novo-cliente.html',
                controller: 'EditarClienteCtrl'
            });

        $urlRouterProvider.otherwise('/sign-in');
    })


.controller('SignInCtrl', function($scope, $state, $rootScope) {
        console.log('SignInCtrl');
        $scope.signIn = function(user) {
            console.log('Sign-In', user);
            Parse.User.logIn(user.username, user.password, {
                success: function(user) {
                    console.log("login success");
                    console.log(user);
                    console.log(Parse.User.current());
                    Parse.Cloud.run('getLoggedUserRole', {}, {
                        success: function(role) {
                            console.log('getLoggedUserRole success');
                            console.log(role);
                            $state.go('home', role);
                        },
                        error: function(error) {
                            console.log('getLoggedUserRole error');
                            console.log(error);
                        }
                    });
                },
                error: function(user, error) {
                    console.error("login error");
                    console.error(user);
                    console.error(error);
                }
            })
        };
    })
    .controller('HomeCtrl', function($scope, $state, $stateParams, Clientes) {
        console.log('HomeCtrl', $stateParams.role);

        Clientes.all().then(function(data) {
            console.log("response from Clientes.all");
            console.log(data);
            $scope.clientes = [];
            angular.forEach(data, function(value, key) {
                this.push({
                    nome: value.attributes.nome
                });
            }, $scope.clientes);
            console.log('clientes');
            console.log($scope.clientes);
            $scope.$digest();
        });

        console.log('clientesfinal');
        console.log($scope.clientes);

        $scope.logout = function() {
            console.log('logout');
            var r = Parse.User.logOut();
            console.log(r);
            $state.go('signin');
        };
    })

.controller('NovoClienteCtrl', function($scope, $state, $rootScope, Clientes, $ionicPopup) {
    console.log('NovoClienteCtrl');
    $scope.cliente = null;
    $scope.error = null;
    $scope.title = "Cadastrar Novo Cliente";
    $scope.showButtonNovo = true;
    $scope.logout = function() {
        console.log('logout');
        var r = Parse.User.logOut();
        console.log(r);
        $state.go('signin');
    };

    $scope.salvarNovoCliente = function(cliente) {
        console.log('salvarNovoCliente', cliente);
        $scope.error = null;
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cadastrar Cliente',
            template: 'Confirma?'
        });
        confirmPopup.then(function(res) {
            if (res) {
                console.log('salvando cliente: ', cliente);
                Parse.Cloud.run('cadastrarCliente', cliente, {
                    success: function() {
                        console.log('cadastrarCliente success');
                        $state.go('home');
                    },
                    error: function(error) {
                        console.log('cadastrarCliente error');
                        console.log(error);
                        $scope.error = error.message;
                        console.log($scope.error);
                        $state.reload();
                    }
                });
            } else {
                console.log('You are not sure');
                console.log(res);
            }
        });
    };
})

.controller('EditarClienteCtrl', function($scope, $state, $rootScope, Clientes, $ionicPopup, $stateParams) {
        console.log('EditarClienteCtrl', $stateParams.id);
        $scope.cliente = {nome: "leo"};
        $scope.error = null;
        $scope.title = "Atualizar Cliente";
        $scope.showButtonEditar = true;
        $scope.logout = function() {
            console.log('logout');
            var r = Parse.User.logOut();
            console.log(r);
            $state.go('signin');
        };

    })
    .controller('MainCtrl', function($scope, $state, $rootScope, Clientes) {
        console.log('MainCtrl');

        $scope.editarCliente = function(cliente_id) {
            console.log('editarCliente', cliente_id);
        };

        $scope.removerCliente = function(cliente_id) {
            console.log('removerCliente', cliente_id);
        };

        $scope.carregarClientes = function() {
            console.log('carregarClientes');
            var query = new Parse.Query(Parse.Object.extend("Cliente"));
            query.ascending("nome");
            query.find({
                success: function(results) {
                    console.log('success getting clients');
                    console.log(results);
                    console.log(results[0].get('nome'));
                    $scope.clientes = results;
                },
                error: function(error) {
                    console.error('error getting clients' + error);
                }
            });
        };
    })
    .factory('Clientes', function() {
        return {
            all: function() {
                console.log('Clientes all');
                var query = new Parse.Query(Parse.Object.extend("Cliente"));
                query.ascending("nome");
                return query.find();
            }
        };
    })
    .factory('Params', function() {
        var _params = null;
        return {
            set: function(params) {
                console.log('set params', params);
                _params = params;
            },
            get: function() {
                console.log('ret params', params);
                _params = null;
                return _params;
            }
        };
    });