Parse.Cloud.beforeSave(Parse.User, function(request, response) {
    response.success();
});

Parse.Cloud.define("cadastrarCliente", function(request, response) {
    Parse.Cloud.useMasterKey();
    console.log("define cadastrarCliente");
    console.log(request);
    var query = (new Parse.Query(Parse.Role));
    query.equalTo("name", "admin");
    query.equalTo("users", request.user);
    query.first().then(function(adminRole) {
        if (adminRole) {
            console.log("user is admin");

            var user = new Parse.User();
            user.set("username", request.params.username);
            user.set("password", request.params.password);
            user.set("email", request.params.email);
            console.log("user is admin");
            user.signUp(null, {
                success: function(user) {
                    console.log("user created");

                    var query = (new Parse.Query(Parse.Role));
                    query.equalTo("name", "cliente");
                    query.first().then(function(clienteRole) {
                        clienteRole.relation("users").add(user);
                        clienteRole.save();
                        console.log("clienteRole.save();");
                    });

                    var Cliente = Parse.Object.extend("Cliente");
                    var cliente = new Cliente();
                    cliente.set("nome", request.params.nome);
                    cliente.set("contato", request.params.contato);
                    cliente.set("telefone", request.params.telefone);
                    cliente.set("email", request.params.email);
                    cliente.set("creator", Parse.User.current());
                    cliente.set("usuario", user);
                    cliente.save();
                    response.success();
                },
                error: function(user, error) {
                    console.error("erro criando usuario");
                    response.error(error.message);
                }
            });

        } else {
            console.error("user is not admin");
            response.error("usuario nao é administrador")
        }
    });
});

Parse.Cloud.define("getLoggedUserRole", function(request, response) {
    console.log("define getLoggedUserRole");
    var query = new Parse.Query(Parse.Role);
    query.equalTo("users", request.user);
    query.find({
        success: function(results) {
            console.log("user role:");
            var userRoleName = results[0].get('name');
            console.log(userRoleName);
            response.success({
                role: userRoleName
            })
        },

        error: function(error) {
            console.error("error gettig user role");
            console.error(error.message);
            response.error(error.message);
        }
    });
});